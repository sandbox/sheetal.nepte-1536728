$Id:

Ubercart Reseller
====================
This module is developed based on Ubercart and Ubercart Marketplace module. I have to develop functionality for my client where reseller can sell website products.
Whenever product is purchased by the user who is registered under reseller, reseller gets the commission.
It has reports like Order, Sales, Income report for each reseller.


Installation Instructions:
==========================
- Extract Ubercart Reseller in your sites/all/modules folder in Drupal directory.
- Enable the module

Dependencies
=============
- Ubercart
- Content Profile

Configuration Instructions:
==========================
- Create Reseller Role
- Apply the reseller permissions
- Create the Reseller profile using content profile module
- Create the Client profile and link reseller profile using node reference (This user will purchase the website products)
- Goto admin/store/settings/reseller/cprofile
- Select the content profile Client and enter the node reference field name

Commission configuration:
=========================
- Goto admin/store/settings/reseller/commission 
- Enter the commission percentage for all the products and save

Reseller Reports:
=================
Income report, Order Report and Product sales report under the reseller
- For Login user
admin/store/reports/reseller/reseller-income
admin/store/reports/reseller/reseller-order
admin/store/reports/reseller/reseller-product

- For specific reseller, % will be the user id of the reseller
admin/store/reports/reseller/reseller-income/2012/%
admin/store/reports/reseller/reseller-order/%
admin/store/reports/reseller/reseller-product/%




