<?php

//view order under all reseller
function ubercart_reseller_reports_all_order($cuid){
//echo "<pre>";
	
$sql ="";
//global $user; //comment later
//print_r($user->uid);

//$cuid = $user->uid; //7
$cuser = user_load($cuid);  
//$cuser = $user;

  drupal_set_title(t('Reseller Orders Reports - '. ucfirst($cuser->name)));
 // $breadcrumb = drupal_get_breadcrumb();
 //$breadcrumb[] = l(t('Selling'), 'user/'. arg(1) .'/selling');
 // drupal_set_breadcrumb($breadcrumb);
 

  $header = array(
    array('data' => t('ID'), 'field' => 'o.order_id'),
    array('data' => t('Product(s)*')),
    array('data' => t('Name')),
    array('data' => t('Total*')),
    array('data' => t('Date'), 'field' => 'o.created', 'sort' => 'desc'),
    array('data' => t('Status'), 'field' => 'sos.order_status'),
    array('data' => t('Actions'))
  ); 

 $result = pager_query("SELECT sos.order_id, o.created, sos.order_status FROM {ubercart_reseller_order} AS sos INNER JOIN {uc_orders} as o ON sos.order_id = o.order_id WHERE (sos.reseller_id = %d) AND ((o.order_status = 'payment_received') OR (o.order_status = 'completed') OR (o.order_status = 'processing'))"  . tablesort_sql($header), 20, 0, "SELECT COUNT(DISTINCT order_id) FROM {ubercart_reseller_order} WHERE reseller_id = %d", $cuid, $cuid);

//fetch data
 while ($order = db_fetch_object($result)) {

	    $productresult = db_query("SELECT p.title, p.nid, p.model, p.price, up.shippable, p.qty, p.order_product_id FROM {uc_order_products} AS p INNER JOIN {node} AS n INNER JOIN {uc_products} as up ON p.nid = n.nid AND up.nid = p.nid AND up.vid = n.vid WHERE order_id = %d", $order->order_id);

    while ($product = db_fetch_object($productresult)) {
      $temp = l($product->title, 'node/'. $product->nid) .' ['. $product->model .'] @ '. uc_currency_format($product->price) .'<br/>';
      if ($product->shippable) {
        $products .= '<div class="shippable">'. $temp .'</div>';
      } 
      else {
        $products .= '<div class="not_shippable">'. $temp .'</div>';
      }
      $total += $product->qty * $product->price;
//print_r($product);
    }

 $rows[] = array(
      array('data' => $order->order_id),
      array('data' => $products),
      array('data' => ucfirst($cuser->name)),
      array('data' => uc_currency_format($total)),
      array('data' => format_date($order->created, 'custom', variable_get('uc_date_format_default', 'm/d/Y'))),
      array('data' => ucwords(t($order->order_status))),
      array('data' => l('View order', 'admin/store/orders/'.$order->order_id))
    );
}

  $output .= theme('table', $header, $rows) . theme('pager', NULL, 20, 0);
  //$output .= '<p>'. t('*The products listed for each order only include the products that you sell.  The total field doesn\'t represent what the customer actually paid, but rather the amount you will be paid for the order.') .'</p>';

return $output;

}


//products summary under reseller
function ubercart_reseller_reports_all_products($uid) {
$cuser = user_load($uid);  
  //$uid = $user->uid;
//$uid = 7;
  drupal_set_title(t('Reseller Sales Reports - '). ucfirst($cuser->name));
 // $breadcrumb = drupal_get_breadcrumb();
 // $breadcrumb[] = l(t('Selling'), 'user/'. arg(1) .'/selling');
//  $breadcrumb[] = l(t('Reports'), 'user/'. arg(1) .'/selling/reports');
 // drupal_set_breadcrumb($breadcrumb);
  
  $statistics = db_result(db_query("SELECT status FROM {system} WHERE name = 'statistics'"));
  $count_views = variable_get('statistics_count_content_views', FALSE);
  $page = (!is_null($_GET['page'])) ? intval($_GET['page']) : 0;
  $page_size = (!is_null($_GET['nopage'])) ? UC_REPORTS_MAX_RECORDS : variable_get('ubercart_reseller_table_size', 30);
  $order_statuses = "('complete')";
  $product_types = array("'product'");
  $types = db_query("SELECT DISTINCT(pcid) FROM {uc_product_classes}");
  $csv_rows = array();
  while ($type = db_fetch_object($types)) {
    $product_types[] = "'". $type->pcid ."'";
  }
//print implode(", ", $product_types);
  if ($statistics && $count_views) { 
    $header = array(
      array('data' => t('#')),
      array('data' => t('Product'), 'field' => 'n.title'),
      array('data' => t('Views'), 'field' => 'c.totalcount'),
      array('data' => t('Sold'), 'field' => 'sold'),
      array('data' => t('Total'), 'field' => 'total', 'sort' => 'desc'),
    );
    $csv_rows[] = array(t('#'), t('Product'), t('Views'), t('Sold'), t('Total'));

    $sql = '';
    switch ($GLOBALS['db_type']) {
      case 'mysqli':
      case 'mysql':
        $sql = "SELECT n.nid, n.title, c.totalcount, (SELECT SUM(qty) FROM {uc_order_products} AS p LEFT JOIN {ubercart_reseller_order} AS o ON p.order_id = o.order_id WHERE o.order_status IN $order_statuses AND p.nid = n.nid AND o.reseller_id = %d) AS sold, (SELECT (SUM(p2.cost * p2.qty)) FROM {uc_order_products} AS p2 LEFT JOIN {ubercart_reseller_order} AS o2 ON p2.order_id = o2.order_id WHERE o2.order_status IN $order_statuses AND p2.nid = n.nid AND o2.reseller_id = %d) AS total FROM {node} AS n LEFT JOIN {node_counter} AS c ON n.nid = c.nid WHERE type IN (". implode(", ", $product_types) .") GROUP BY n.nid DESC";
        break;
      case 'pgsql':
        $sql = "SELECT n.nid, n.title, c.totalcount, (SELECT SUM(qty) FROM {uc_order_products} AS p LEFT JOIN {ubercart_reseller_order} AS o ON p.order_id = o.order_id WHERE o.order_status IN $order_statuses AND p.nid = n.nid AND o.reseller_id = %d) AS sold, (SELECT (SUM(p2.cost * p2.qty)) FROM {uc_order_products} AS p2 LEFT JOIN {ubercart_reseller_order} AS o2 ON p2.order_id = o2.order_id WHERE o2.order_status IN $order_statuses AND p2.nid = n.nid AND o2.reseller_id = %d) AS total FROM {node} AS n LEFT JOIN {node_counter} AS c ON n.nid = c.nid WHERE type IN (". implode(", ", $product_types) .") GROUP BY n.nid";
        break;
    }
  }
  else { 
    $header = array(
      array('data' => t('#')),
      array('data' => t('Product'), 'field' => 'n.title'),
      array('data' => t('Sold'), 'field' => 'sold'),
      array('data' => t('Total'), 'field' => 'total', 'sort' => 'desc'),
    );
    $csv_rows[] = array(t('#'), t('Product'), t('Sold'), t('Total'));

    switch ($GLOBALS['db_type']) {
      case 'mysqli':
      case 'mysql':
        $sql = "SELECT n.nid, n.title, (SELECT SUM(qty) FROM {uc_order_products} AS p LEFT JOIN {ubercart_reseller_order} AS o ON p.order_id = o.order_id WHERE o.order_status IN $order_statuses AND p.nid = n.nid AND o.reseller_id = %d) AS sold, (SELECT (SUM(p2.price * p2.qty)) FROM {uc_order_products} AS p2 LEFT JOIN {ubercart_reseller_order} AS o2 ON p2.order_id = o2.order_id WHERE o2.order_status IN $order_statuses AND p2.nid = n.nid AND o2.reseller_id = %d) AS total FROM {node} AS n WHERE type IN (". implode(', ', $product_types) .') GROUP BY n.nid DESC';
        break;
      case 'pgsql':
        $sql = "SELECT n.nid, n.title, (SELECT SUM(qty) FROM {uc_order_products} AS p LEFT JOIN {ubercart_reseller_order} AS o ON p.order_id = o.order_id WHERE o.order_status IN $order_statuses AND p.nid = n.nid AND o.reseller_id = %d) AS sold, (SELECT (SUM(p2.price * p2.qty)) FROM {uc_order_products} AS p2 LEFT JOIN {ubercart_reseller_order} AS o2 ON p2.order_id = o2.order_id WHERE o2.order_status IN $order_statuses AND p2.nid = n.nid AND o2.reseller_id = %d) AS total FROM {node} AS n WHERE type IN (". implode(', ', $product_types) .') GROUP BY n.nid, n.title';
        break;
    }
  }

  $sql_count = "SELECT COUNT(*) FROM {node} WHERE type IN (". implode(", ", $product_types) .")";
  $products = pager_query($sql . tablesort_sql($header), $page_size, 0, $sql_count, $uid, $uid, $uid);

  while ($product = db_fetch_array($products)) {
 //print_r($product);

    $row_cell = ($page * variable_get('ubercart_reseller_table_size', 30)) + count($rows) + 1;
    $product_cell = l($product['title'], 'node/'. ($product['nid']));
    $product_csv = $product['title'];
    $sold_cell = (empty($product['sold'])) ? 0 : $product['sold'];
    $sold_csv = $sold_cell;
    $total_cell = uc_currency_format((empty($product['total'])) ? 0 : $product['total']);
    $total_csv = $total_cell;

    if (module_exists('uc_attribute')) {
      $product_models = db_query("SELECT model FROM {uc_product_adjustments} WHERE nid = %d", $product['nid']);
      $models = array(db_result(db_query("SELECT model FROM {uc_products} WHERE nid = %d", $product['nid'])));
      unset($breakdown_product, $breakdown_sold, $breakdown_total);
      while ($product_model = db_fetch_object($product_models)) {
        $models[] = $product_model->model;
    }
    foreach ($models as $model) {
      $sold = db_result(db_query("SELECT SUM(qty) FROM {uc_order_products} AS p LEFT JOIN {ubercart_reseller_order} AS o ON p.order_id = o.order_id WHERE o.order_status IN $order_statuses AND p.model = '%s' AND p.nid = %d AND o.reseller_id = %d", $model, $product['nid'], $uid));
      $total = db_result(db_query("SELECT SUM(p.price * p.qty) FROM {uc_order_products} AS p LEFT JOIN {ubercart_reseller_order} AS o ON p.order_id = o.order_id WHERE o.order_status IN $order_statuses AND p.model = '%s' AND p.nid = %d AND o.reseller_id = %d", $model, $product['nid'], $uid));
      $breakdown_product .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$model";
      $product_csv .= "\n     $model";
      $breakdown_sold .= "<br/>". ((!empty($sold)) ? $sold : 0);
      $sold_csv .= "\n     ". ((!empty($sold)) ? $sold : 0);
      $breakdown_total .= "<br/>". (uc_currency_format((!empty($total)) ? $total : 0));
      $total_csv .= "\n     ". (uc_currency_format((!empty($total)) ? $total : 0));
    }
    $product_cell = $product_cell . $breakdown_product;
    $sold_cell = '<strong>'. $sold_cell .'</strong>'. $breakdown_sold;
    $total_cell = '<strong>'. $total_cell .'</strong>'. $breakdown_total;
  }
  
  if ($statistics && $count_views) {
    $views = (empty($product['totalcount'])) ? 0 : $product['totalcount'];
    $rows[] = array(
      array('data' => $row_cell),
      array('data' => $product_cell),
      array('data' => $views),
      array('data' => $sold_cell),
      array('data' => $total_cell, 'nowrap' => 'nowrap')
    );
    //  $csv_rows[] = array($row_cell, $product_csv, $views, $sold_csv, $total_csv);
  }
  else {
    $rows[] = array(
      array('data' => $row_cell),
      array('data' => $product_cell),
      array('data' => $sold_cell),
      array('data' => $total_cell, 'nowrap' => 'nowrap')
    );
 //   $csv_rows[] = array($row_cell, $product_csv, $sold_csv, $total_csv);
    }
  }
  
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No products found'), 'colspan' => count($header)));
  }
 // $csv_data = ubercart_reseller_store_csv('ubercart_reseller_products', $csv_rows);

  $output = theme('table', $header, $rows, array('width' => '100%', 'class' => 'uc-sales-table'));
  $output .= theme_pager(NULL, $page_size);
//  $output .= '<div class="uc-reports-links">'. l(t('Export to csv file.'), 'user/'. $uid .'/selling/reports/gettxt/'. $csv_data['report'] .'/'. $csv_data['user']) .'</div>';

  return $output;
}

/*



SELECT o.order_id, o.uid, o.billing_first_name, o.billing_last_name, o.order_total, o.order_status, o.created, os.title 
FROM uc_orders o LEFT JOIN uc_order_statuses os ON o.order_status = os.order_status_id



*/

// Form builder for the custom product report.
function ubercart_reseller_products_custom_form(&$form_state, $values, $statuses) {
  $form = array();

  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Customize product report parameters'),
    '#description' => t('Adjust these values and update the report to build your custom product report. Once submitted, the report may be bookmarked for easy reference in the future.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['search']['start_date'] = array(
    '#type' => 'date',
    '#title' => t('Start date'),
    '#default_value' => array(
      'month' => format_date($values['start_date'], 'custom', 'n'),
      'day' => format_date($values['start_date'], 'custom', 'j'),
      'year' => format_date($values['start_date'], 'custom', 'Y'),
    ),
  );
  $form['search']['end_date'] = array(
    '#type' => 'date',
    '#title' => t('End date'),
    '#default_value' => array(
      'month' => format_date($values['end_date'], 'custom', 'n'),
      'day' => format_date($values['end_date'], 'custom', 'j'),
      'year' => format_date($values['end_date'], 'custom', 'Y'),
    ),
  );

  $options = array();
  foreach (ubercart_reseller_order_status_list() as $status) {
    $options[$status['id']] = $status['title'];
  }

  if ($statuses === FALSE) {
    $statuses = variable_get('ubercart_reseller_reported_statuses', array('completed'));
  }

  $form['search']['status'] = array(
    '#type' => 'select',
    '#title' => t('Order statuses'),
    '#description' => t('Only orders with selected statuses will be included in the report.') .'<br />'. t('Hold Ctrl + click to select multiple statuses.'),
    '#options' => $options,
    '#default_value' => $statuses,
    '#multiple' => TRUE,
    '#size' => 5,
  );

  $form['search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update report'),
  );

  return $form;
}

/**
 * Return a sorted list of order statuses, sortable by order state/scope.
 *
 * @param $scope
 *   Specify the scope for the order statuses you want listed - all, general,
 *   specific, or any order state id.  Defaults to all.
 * @param $sql
 *   Pass this parameter as TRUE to alter the return value for a SQL query.
 * @param $action
 *   Empty by default.  Set to rebuild to load the order statuses from scratch,
 *   disregarding the current cached value for the specified $scope.
 * @return
 *   Either an array of status arrays or a string containing an array of status
 *   ids for use in a SQL query.
 */
function ubercart_reseller_order_status_list($scope = 'all', $sql = FALSE, $action = '') {
  static $statuses;

  if (!isset($statuses[$scope]) || $action == 'rebuild') {
    switch ($scope) {
      case 'all':
        $result = db_query("SELECT * FROM {uc_order_statuses}");
        break;
      case 'general':
      case 'specific':
        $result = db_query("SELECT * FROM {uc_order_statuses} WHERE state IN "
                         . ubercart_reseller_order_state_list($scope, TRUE));
        break;
      default:
        $result = db_query("SELECT * FROM {uc_order_statuses} WHERE state = '%s'", $scope);
        break;
    }

    $statuses[$scope] = array();
    while ($status = db_fetch_array($result)) {
      $status['id'] = $status['order_status_id'];
      unset($status['order_status_id']);
      $statuses[$scope][] = $status;
    }
    usort($statuses[$scope], 'uc_weight_sort');
  }

  if ($sql) {
    foreach ($statuses[$scope] as $status) {
      $ids[] = $status['id'];
    }
    return "('". implode("', '", $ids) ."')";
  }

  return $statuses[$scope];
}


/**
 * Displays the yearly sales report for sellers.
 */
function _ubercart_reseller_report_sales($uid) {
$cuser = user_load($uid);
  //$uid = $user->uid;
  drupal_set_title(t('Reseller Yearly Sales Reports - ') .ucfirst($cuser->name));
 // $breadcrumb = drupal_get_breadcrumb();
 // $breadcrumb[] = l(t('Selling'), 'user/'. arg(1) .'/selling');
 // $breadcrumb[] = l(t('Reports'), 'user/'. arg(1) .'/selling/reports');
//  drupal_set_breadcrumb($breadcrumb);
  
  $timezone_offset = time() + variable_get('date_default_timezone', 0);
  $order_statuses = "('complete')";

  // Get the year for the report from the URL.
//print_r(arg(4));
  if (intval(arg(5)) == 0) {
    $year = format_date($timezone_offset, 'custom', 'Y', 0);
  }
  else {
    $year = arg(5);
  }

  // Build the header for the report table.
  $header = array(t('Month'), t('Number of orders'), t('Total revenue'), t('Average order'));

  // Build the header to the csv export.
  $csv_rows = array(array(t('Month'), t('Number of orders'), t('Total revenue'), t('Average order')));

  // For each month of the year...
  for ($i = 1; $i <= 12; $i++) {
    // Calculate the start and end timestamps for the month in local time.
    $month_start = gmmktime(0, 0, 0, $i, 1, $year);
    $month_end = gmmktime(23, 59, 59, $i + 1, 0, $year);

    // Get the sales report for the month.
    $month_sales = _ubercart_reseller_get_all_sales($uid, $month_start, 'month');

    // Calculate the average order total for the month.
    if ($month_sales['total'] != 0) {
      $month_average = round($month_sales['income'] / $month_sales['total'], 2);
    }
    else {
      $month_average = 0;
    }

    // Add the month's row to the report table.
    $rows[] = array(
      gmdate('M Y', $month_start),
      $month_sales['total'],
      uc_currency_format($month_sales['income']),
      uc_currency_format($month_average),
    );

    // Add the data to the csv export.
    $csv_rows[] = array(
      gmdate('M Y', $month_start),
      $month_sales['total'],
      uc_currency_format($month_sales['income']),
      uc_currency_format($month_average),
    );
  }

  // Calculate the start and end timestamps for the year in local time.
  $year_start = gmmktime(0, 0, 0, 1, 1, $year);
  $year_end = gmmktime(23, 59, 59, 1, 0, $year + 1);

  // Get the sales report for the year.
  $year_sales = _ubercart_reseller_get_all_sales($uid, $year_start, 'year');

  // Calculate the average order total for the year.
  if ($year_sales['total'] != 0) {
    $year_average = round($year_sales['income'] / $year_sales['total'], 2);
  }
  else {
    $year_average = 0;
  }

  // Add the total row to the report table.
  $rows[] = array(
    t('Total @year', array('@year' => $year)),
    $year_sales['total'],
    uc_currency_format($year_sales['income']),
    uc_currency_format($year_average),
  );

  // Add the total data to the csv export.
  $csv_rows[] = array(
    t('Total @year', array('@year' => $year)),
    $year_sales['total'],
    uc_currency_format($year_sales['income']),
    uc_currency_format($year_average),
  );

  // Cache the csv export.
  //$csv_data = uc_reports_store_csv('mp_seller_sales_yearly', $csv_rows);

  // Build the page output holding the form, table, and csv export link.
  $output = drupal_get_form('ubercart_reseller_sales_year_form', $year)
    . theme('table', $header, $rows, array('width' => '100%', 'class' => 'uc-sales-table'));
 //   .'<div class="uc-reports-links">'. l(t('Export to csv file.'), 'user/'. $uid .'/selling/reports/gettxt/'. $csv_data['report'] .'/'. $csv_data['user']) .'</div>';

  return $output;
}


/**
 * Overrides _uc_reports_get_sales().
 * @see _uc_reports_get_sales(). *
 */
function _ubercart_reseller_get_all_sales($uid, $time, $period = 'day') {
  $output = array();
  $output['income'] = 0;
  $order_statuses = "('complete')";
  $totalorders = array();
  // Build the report table header.
  //$header = array(t('Sales data'), t('Number of orders'), t('Total revenue'), t('Average order'));

//print format_date($time, 'custom', 'd');
//print $period. " " . $uid;

  switch ($GLOBALS['db_type']) {
    case 'mysqli':
    case 'mysql':
      switch ($period) {
        default:
        case 'day':
          $output['date'] = format_date($time, 'custom', 'n') .'-'. format_date($time, 'custom', 'j');
          $orders = db_query("SELECT p.price, p.qty, sos.order_id FROM {ubercart_reseller_order} AS sos LEFT JOIN {uc_orders} AS o ON sos.order_id = o.order_id LEFT JOIN {uc_order_products} AS p ON p.order_id = o.order_id LEFT JOIN {node} AS n ON n.nid = p.nid WHERE sos.order_status IN $order_statuses AND sos.reseller_id = %d AND FROM_UNIXTIME(o.created) LIKE \"". format_date($time, 'custom', 'Y') .'-'. format_date($time, 'custom', 'm') .'-'. format_date($time, 'custom', 'd') ."%%\"", $uid, $uid);
        break;
        case 'month':
          $output['date'] = format_date($time, 'custom', 'n');
          $orders = db_query("SELECT p.price, p.qty, sos.order_id FROM {ubercart_reseller_order} AS sos LEFT JOIN {uc_orders} AS o ON sos.order_id = o.order_id LEFT JOIN {uc_order_products} AS p ON p.order_id = o.order_id LEFT JOIN {node} AS n ON n.nid = p.nid WHERE sos.order_status IN $order_statuses AND sos.reseller_id = %d AND FROM_UNIXTIME(o.created) LIKE \"". format_date($time, 'custom', 'Y') .'-'. format_date($time, 'custom', 'm') ."%%\"", $uid, $uid);
        break;
        case 'year':
          $output['date'] = format_date($time, 'custom', 'Y');
          $orders = db_query("SELECT p.price, p.qty, sos.order_id FROM {ubercart_reseller_order} AS sos LEFT JOIN {uc_orders} AS o ON sos.order_id = o.order_id LEFT JOIN {uc_order_products} AS p ON p.order_id = o.order_id LEFT JOIN {node} AS n ON n.nid = p.nid WHERE sos.order_status IN $order_statuses AND sos.reseller_id = %d AND FROM_UNIXTIME(o.created) LIKE \"". format_date($time, 'custom', 'Y') ."-%%\"", $uid, $uid);
        break;
      }
   break;
     case 'pgsql':
       switch ($period) {
        default:
        case 'day':
          $output['date'] = format_date($time, 'custom', 'n') .'-'. format_date($time, 'custom', 'j');
          $orders = db_query("SELECT p.price, p.qty, sos.order_id FROM {ubercart_reseller_order} AS sos LEFT JOIN {uc_orders} AS o ON sos.order_id = o.order_id LEFT JOIN {uc_order_products} AS p ON p.order_id = o.order_id LEFT JOIN {node} AS n ON n.nid = p.nid WHERE sos.order_status IN $order_statuses AND sos.reseller_id = %d AND FROM_UNIXTIME(o.created) LIKE \"". format_date($time, 'custom', 'Y') .'-'. format_date($time, 'custom', 'm') .'-'. format_date($time, 'custom', 'd') ."%%\"", $uid, $uid);
        break;
        case 'month':
          $output['date'] = format_date($time, 'custom', 'n');
          $orders = db_query("SELECT p.price, p.qty, sos.order_id FROM {ubercart_reseller_order} AS sos LEFT JOIN {uc_orders} AS o ON sos.order_id = o.order_id LEFT JOIN {uc_order_products} AS p ON p.order_id = o.order_id LEFT JOIN {node} AS n ON n.nid = p.nid WHERE sos.order_status IN $order_statuses AND sos.reseller_id = %d AND FROM_UNIXTIME(o.created) LIKE \"". format_date($time, 'custom', 'Y') .'-'. format_date($time, 'custom', 'm') ."%%\"", $uid, $uid);
        break;
        case 'year':
          $output['date'] = format_date($time, 'custom', 'Y');
          $orders = db_query("SELECT p.price, p.qty, sos.order_id FROM {ubercart_reseller_order} AS sos LEFT JOIN {uc_orders} AS o ON sos.order_id = o.order_id LEFT JOIN {uc_order_products} AS p ON p.order_id = o.order_id LEFT JOIN {node} AS n ON n.nid = p.nid WHERE sos.order_status IN $order_statuses AND sos.reseller_id = %d AND FROM_UNIXTIME(o.created) LIKE \"". format_date($time, 'custom', 'Y') ."-%%\"", $uid, $uid);
        break;
      }
    break;
  }



  while ($order = db_fetch_object($orders)) {
	//print_r($order);
    $output['income'] += $order->price * $order->qty;
    $totalorders[$order->order_id] = 1;
  }
  
  $output['total'] = sizeof($totalorders);
  $output['total'] = (!empty($output['total'])) ? $output['total'] : 0;
  $output['average'] = ($output['total'] != 0) ? round($output['income'] / $output['total'], 2) : 0;

    /*Add the month's row to the report table.
    $rows[] = array(
      gmdate('M Y', $month_start),
      $month_sales['total'],
      uc_currency_format($month_sales['income']),
      uc_currency_format($month_average), 
	);

  $output_str = drupal_get_form('ubercart_reseller_sales_year_form', $year)
    . theme('table', $header, $rows, array('width' => '100%', 'class' => 'uc-sales-table'));
 //   .'<div class="uc-reports-links">'. l(t('Export to csv file.'), 'user/'. $uid .'/selling/reports/gettxt/'. $csv_data['report'] .'/'. $csv_data['user']) .'</div>';

  */

  return $output;
}

/**
 * Form to specify a year for the yearly sales report.
 */
function ubercart_reseller_sales_year_form($form_state, $year) {
  $form['year'] = array(
    '#type' => 'textfield',
    '#title' => t('Sales year'),
    '#default_value' => $year,
    '#maxlength' => 4,
    '#size' => 4,
    '#prefix' => '<div class="sales-year">',
    '#suffix' => '</div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('View'),
    '#prefix' => '<div class="sales-year">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Submit handler for custom year selection.
 */
function ubercart_reseller_sales_year_form_submit($form, &$form_state) {
	//print_r($form);
 	if (intval(arg(6)) == 0) {
  		drupal_goto('admin/store/reports/reseller/reseller-income/'. $form_state['values']['year']);
	} else{
	  drupal_goto('admin/store/reports/reseller/reseller-income/'. $form_state['values']['year']. '/'.arg(6));
	}
}

//commission setting form post
function ubercart_reseller_commission_form_submit($form, &$form_state) {
  $classes_array = module_invoke_all('product_types');
  $roles_array = user_roles(true, 'act as reseller');
  foreach ($classes_array as $class) {
    foreach ($roles_array as $role) {
      db_query("UPDATE {ubercart_reseller_rate} SET rate = %f WHERE rid = %d AND class = '%s'", $form_state['values'][$class][$role], array_search($role, $roles_array), $class);
      if (!db_affected_rows()) {
        db_query("INSERT INTO {ubercart_reseller_rate} (rate, rid, class) VALUES (%f, %d, '%s')", $form_state['values'][$class][$role], array_search($role, $roles_array), $class);
      }
    }
  }
  drupal_set_message(t('Rates saved.'));
}

//commission setting form
function ubercart_reseller_commission_form() {
  $roles_array = user_roles(true, 'act as reseller');
  $classes_array = module_invoke_all('product_types');
  $form = array();
  $form['#tree'] = TRUE;
//print_r($classes_array);
//print_r($roles_array);
  foreach ($classes_array as $class) {
    $form[$class]['title'] = array('#value' => t('Product Type: ') .ucfirst($class));
    foreach ($roles_array as $role) {
      $form[$class][$role] = array(
        '#type' => 'textfield',
        '#description' => t('Role: ') . ucfirst($role),
        '#size' => 10,
        '#default_value' => variable_get('ubercart_reseller_commission_rate', '0.20')
      );
    }
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  return $form;

}


function ubercart_reseller_settings_form_submit($form, &$form_state){
	variable_set('ubercart_reseller_cprofile', $form_state["values"]["ubercart_reseller_cprofile"]);
	variable_set('ubercart_reseller_refer_by', $form_state["values"]["ubercart_reseller_refer_by"]);

  drupal_set_message(t('Settings saved.'));
}

//reseller setting form
function ubercart_reseller_settings_form() {
  $form['ubercart_reseller_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => 'Reseller form settings'
  );
  $form['ubercart_reseller_fieldset']['ubercart_reseller_cprofile'] = array(
      '#type' => 'select',
      '#title' => t('Select Content Profile'),
      '#description' => t('Select the user content profile content type where referred reseller id is stored, It will be used while user is purchasing the products'),
      '#default_value' => variable_get('ubercart_reseller_cprofile', ''),
	  '#options' => content_profile_get_types('names'),
  );

  $form['ubercart_reseller_fieldset']['ubercart_reseller_refer_by'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter field name used for reference id'),
      '#description' => '',
      '#default_value' => variable_get('ubercart_reseller_refer_by', ''),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  return $form;
}



?>
